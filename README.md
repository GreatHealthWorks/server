# Server for simple demo app
Technologies Used
- [x] **Node.js** with **Express**
- [x] **MongoDb** with **Mongoose**
- [x] **Socket.io**
- [x] **JWT**

### To run the server
`npm install` to install dependencies

`node index.js` to run your local server or just do `nodemon` for development

### Post collection
[please find here all api calls](/postman/Test.postman_collection.json)
> After calling login the token will be set automatically for secure api calls




