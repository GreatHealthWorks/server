const mongoose = require('mongoose');

const messageSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    text:String,
    from:String,
    created:{ type: Date, default: Date.now },
});

module.exports = mongoose.model('Message', messageSchema);