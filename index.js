const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const mongoose = require('mongoose');
const cors = require('cors');
const router = express.Router();
const Message = require('./models/message');
const User = require('./models/user');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
const bcryptSaltRounds = 10;
const jwtSecretKey = "SuperSecurejwtSecretKey";

mongoose.connect(process.env.MongoDbAtlasConnectionUrl || 'mongodb+srv://chatUser:chatUser@chat-jjakx.mongodb.net/test?retryWrites=true');

// Authorization: Bearer <access_token>
const tokenVerified = (req, res, next) => {
    const bearerHeader = req.headers['authentication'];
    if (bearerHeader) {
        const bearerWords = bearerHeader.split(' ');
        if (bearerWords.length != 2 || bearerWords[0] !== 'Bearer') {
            res.sendStatus(403);
        }
        else {
            const bearerToken = bearerWords[1];
            req.token = bearerToken;
            next();
        }
    } else {
        // Forbidden
        res.sendStatus(403);
    }
};

const openToken = (token) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, jwtSecretKey, (err, authData) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(authData);
            }
        });
    });
};

router.get('/', function (req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

router.get('/users', function (req, res) {
    const page = Number(req.query.cursor || 0);
    const pageSize = Number(req.query.pageSize || 10);
    User.find()
        .limit(pageSize)
        .skip(pageSize * page)
        .sort({ nickname: 'asc' }).exec()
        .then((usersWithPassData) => {
            let users = usersWithPassData.map((u) => ({ nickname: u.nickname, avatarURL: u.avatarURL }));
            let cursorResponse = (users || []).length < pageSize ? null : page + 1;
            res.json({ items: users, cursor: cursorResponse });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json(err);
        });
});

router.get('/chats', tokenVerified, function (req, res) {
    const page = Number(req.query.cursor || 0);
    const pageSize = Number(req.query.pageSize || 10);
    openToken(req.token)
        .then((userData) => {
            const nickname = userData.nickname;
            Message.find()
                .limit(pageSize)
                .skip(pageSize * page)
                .sort({ created: 'desc' }).exec()
                .then((messages) => {
                    let messageResponds = getMessageResponds(messages, nickname);
                    let cursorResponse = (messageResponds || []).length < pageSize ? null : page + 1;
                    res.json({ items: messageResponds, cursor: cursorResponse });
                })
                .catch((err) => {
                    console.log(err);
                    res.status(500).json(err);
                });
        })
        .catch((err) => {
            res.sendStatus(403).json(err);
        });
});

router.post('/chat', tokenVerified, function (req, res) {
    const messageText = req.body.msg;
    const socketId = req.body.socketId;
    console.log("socketId",socketId);
    openToken(req.token)
        .then((userData) => {
            const message = new Message({
                _id: new mongoose.Types.ObjectId(),
                text: messageText,
                from: userData.nickname || 'anonymous',
            });
            if(socketId)
            {
                let socket = io.sockets.connected[socketId];
                if(socket)
                {
                    socket.broadcast.emit('receive-message', message);
                    let messageFromMe = {
                        _id: message._id,
                        text: message.text,
                        from: message.from,
                        created: message.created,
                        fromMe: true,
                    };
                    socket.emit('receive-message', messageFromMe);
                }
                else{
                    io.emit('receive-message', message);
                }
            }
            else{
                io.emit('receive-message', message);
            }
            message.save()
                .then(() => res.json(message))
                .catch((err) => res.status(500).json(err));
        })
        .catch((err) => {
            res.sendStatus(403).json(err);
        });

});

router.post('/login', (req, res) => {
    const nickname = req.body.nickname;
    const password = req.body.password;//ideally the client will not send the password on the wire
    User.findOne({ nickname }).exec()
        .then(user => {
            if (user) {
                const hash = user.hashPassword;
                bcrypt.compare(password, hash, (err, match) => {
                    if (match) {
                        jwt.sign({ nickname: nickname }, jwtSecretKey, (err, token) => {
                            if (err) {
                                res.status(403);
                            }
                            res.json({
                                token
                            });
                        });
                    }
                    else {
                        res.status(403).json("Invalid nickname or password");//bad password
                    }
                });
            }
            else {
                res.status(403).json("Invalid nickname or password");/*Wrong nickname*/
            }
        })
        .catch((err) => res.status(500).json(err));
});

router.post('/register', (req, res) => {
    const nickname = req.body.nickname || "";
    const password = req.body.password || "";//ideally the client will not send the password on the wire
    const avatarURL = req.body.avatarURL || 'https://randomuser.me/api/portraits/thumb/men/23.jpg';
    if (nickname.length < 3 || password.length < 3) {
        res.status(400).json("Nickname and password must be at least 3 letters long");
    }
    else {
        User.findOne({ nickname }).exec()
            .then((user) => {
                if (user) {
                    res.status(403).json("Nickname already in use");
                }
                else {
                    bcrypt.hash(password, bcryptSaltRounds, (err, hashPassword) => {
                        if (err) {
                            res.status(500).json(err)
                        }
                        else {
                            const user = new User({
                                _id: new mongoose.Types.ObjectId(),
                                nickname,
                                hashPassword,
                                avatarURL,
                            });
                            user.save()
                                .then(() => res.json({ nickname, avatarURL }))
                                .catch((err) => res.status(500).json(err));
                        }
                    });
                }
            })
            .catch((err) => res.status(500).json(err));
    }
});

app.use(cors());
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
app.use('/api', router);


// io.on('connection', (socket) => {
// socket.on('join-chat', (nickname) => {
//     socket.nickname = nickname;
//     io.emit('users-changed', { user: nickname, event: 'joined' });
// });

// socket.on('send-message', (messageText) => {
//     const message = new Message({
//         _id: new mongoose.Types.ObjectId(),
//         text: messageText,
//         from: socket.nickname || 'anonymous'
//     });

//     message.save()
//         .then(() => { console.log('Saved Message', message); })
//         .catch(() => console.log(err));
//     io.emit('receive-message', message);
// });
// });

var port = process.env.PORT || 3001;

http.listen(port, function () {
    console.log('listening in http://localhost:' + port);
});

const getMessageResponds = (messages, nickname) => {
    let results = [];
    for (let i = 0; i < (messages || []).length; i++) {
        let msg = messages[i];
        results.push({
            _id: msg._id,
            text: msg.text,
            from: msg.from,
            created: msg.created,
            fromMe: msg.from === nickname,
        });
    }
    return results;
};
